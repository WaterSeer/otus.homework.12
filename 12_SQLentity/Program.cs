﻿using System;

namespace _12_SQLentity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    //Это типо Main
    int Main
    {
        IRepository repo = new Repository();
        var phones = Repository.GetPhonesById(1);
    }

    //Это типо ADO

    public interface IRepository
    {
        List<Phone> GetPhonesById(int Id);
    }

    public class Repository: IRepository
    {
        List<Phone> GetPhoneById(int Id)
        {
            using(var db=new PhoneContext())
            {
                return db.Phones.Where(p => p.CompanyId == Id).ToList();
            }
        }
    }
}
