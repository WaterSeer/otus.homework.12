﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _12_SQLentity
{
    public  class Book
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string Author { get; set; }
        public int Price { get; set; }
    }

    public class BookContext : DbContext
    {
        public BookContext() : base("DefaultConnection") { }
        public DbSet<Book> Books {get;set;}
    }

    interface IRepository<T>:IDisposable where T:class
    {
        IEnumerable<T> GetBookList(); //получение всех объектов
        T GetBook(int id);//получение объекта по id
        void Create(T item);//создание объекта
        void Update(T item);//обновление объекта
        void Delete(int id);//удаление объекта по id
        void Save();//сохранение изменений
    }

    public class SQLBookRepository:IRepository<Book>
    {
        private BookContext db;
        public SQLBookRepository()
        {
            this.db = new BookContext();
        }

        public IEnumerable<Book> GetBookList()
        {
            return db.Books;
        }
        public Book GetBook(int id)
        {
            return db.Books.Find(id);
        }
        public void Create(Book book)
        {
            db.Books.Add(book);
        }

        public void Update(Book book)
        {
            db.Entry(book).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Book book = db.Books.Find(id);
            if (book != null)
                db.Books.Remove(book);
        }
        public void Save()
        {
            db.SaveChanges();


        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }





    }


}
